import os
import psycopg2

# Получение URL базы данных из переменных окружения
database_url = os.environ.get("POSTGRES_URL")

# SQL запрос
query = """
SELECT 
    s.first_name, 
    s.middle_name, 
    s.last_name, 
    s.birth_date 
FROM 
    students s 
JOIN 
    exams e ON s.id = e.student_id 
JOIN 
    subjects sub ON e.subject_id = sub.id 
WHERE 
    sub.name = 'Математика' 
ORDER BY 
    e.score DESC;
"""

try:
    # Подключение к базе данных
    with psycopg2.connect(database_url) as conn:
        with conn.cursor() as cur:
            cur.execute(query)
            results = cur.fetchall()

    # Вывод результатов
    for row in results:
        print(row)
except psycopg2.OperationalError as e:
    print(f"An error occurred while connecting to the database: {e}")