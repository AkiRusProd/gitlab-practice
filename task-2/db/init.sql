CREATE TABLE students (
    id SERIAL PRIMARY KEY,
    first_name VARCHAR(50),
    middle_name VARCHAR(50),
    last_name VARCHAR(50),
    birth_date DATE
);

CREATE TABLE subjects (
    id SERIAL PRIMARY KEY,
    name VARCHAR(50)
);

CREATE TABLE teachers (
    id SERIAL PRIMARY KEY,
    first_name VARCHAR(50),
    middle_name VARCHAR(50),
    last_name VARCHAR(50)
);

CREATE TABLE exams (
    id SERIAL PRIMARY KEY,
    student_id INTEGER REFERENCES students(id),
    subject_id INTEGER REFERENCES subjects(id),
    teacher_id INTEGER REFERENCES teachers(id),
    exam_date DATE,
    score INTEGER
);


INSERT INTO students (first_name, middle_name, last_name, birth_date) VALUES 
('Иван', 'Иванович', 'Иванов', '2000-01-01'),
('Петр', 'Петрович', 'Петров', '2001-02-02'),
('Сергей', 'Сергеевич', 'Сергеев', '2002-03-03');

INSERT INTO subjects (name) VALUES 
('Математика'),
('Физика'),
('Химия');

INSERT INTO teachers (first_name, middle_name, last_name) VALUES 
('Алексей', 'Алексеевич', 'Алексеев'),
('Борис', 'Борисович', 'Борисов'),
('Виктор', 'Викторович', 'Викторов');

INSERT INTO exams (student_id, subject_id, teacher_id, exam_date, score) VALUES 
(1, 1, 1, '2022-01-01', 85),
(2, 2, 2, '2022-02-02', 90),
(3, 3, 3, '2022-03-03', 95),
(1, 2, 2, '2022-04-04', 80),
(1, 3, 3, '2022-05-05', 75),
(2, 1, 1, '2022-06-06', 85),
(2, 3, 3, '2022-07-07', 90),
(3, 1, 1, '2022-08-08', 95),
(3, 2, 2, '2022-09-09', 100);
