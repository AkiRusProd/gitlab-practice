"""
12. Напишите две функции. Первая функция заполняет массив 
случайными значениями, вторая функция выводит массив на экран
"""

import os
import random

def fill_array(n, min_value=0, max_value=100):
    return [random.randint(min_value, max_value) for _ in range(n)]

def print_array(arr):
    print("---START PRINTING VALUES---")
    for value in arr:
        print(value)
    print("---END PRINTING VALUES---")

if __name__ == "__main__":
    n = int(os.environ.get("N_ELEMS"))
    arr = fill_array(n)
    print_array(arr)